<?php

// Autoload classes
function __autoload($className)
{
    $include = __DIR__ . '/classes/' . $className . '.php';
    /** @noinspection PhpIncludeInspection */
    include_once($include);
}

try {

    // Configuració
    $config = Config::get();

    // Path on guardar les estadístiques
    $pathData = __DIR__ . $config['path_data'];
    if (!file_exists($pathData)) {
        mkdir($pathData, 0777, true);
    }

    // IAmetrics
    if ($config['enable_iametrics']) {
        $statIametrics = new StatIametrics();
        File::output($statIametrics->getLogins(), $pathData . "/iametrics_logins.tab", true, true);
        File::output($statIametrics->getCuentas(), $pathData . "/iametrics_cuentas.tab", true, true);
        File::output($statIametrics->getUsuarios(), $pathData . "/iametrics_usuarios.tab", true, true);
        File::output($statIametrics->getUsuariosAplicacionCuenta(), $pathData . "/iametrics_usuarios_aplicacion_cuenta.tab", true, true);
    }

    // Gps
    if ($config['enable_gps']) {
        $statGPS = new StatGPS();
        File::output($statGPS->getLogins(), $pathData . "/gps_logins.tab", true);
        File::output($statGPS->getDatosPersonales(), $pathData . "/gps_datos_personales.tab", true);
        File::output($statGPS->getCuentas(), $pathData . "/gps_cuentas.tab", true);
        File::output($statGPS->getQueueBDCuentasInc(), $pathData . "/gps_cargas_incrementales.tab", true);
        File::output($statGPS->getUsuariosCuentas(), $pathData . "/gps_usuarios_cuentas.tab", true);
        File::output($statGPS->getPeriodosCuentaCentro(false), $pathData . "/gps_periodos_cuenta_centro.tab", true);
        File::output($statGPS->getPeriodosCuentaCentro(true), $pathData . "/gps_periodos_cuenta_centro_admin.tab", true);
    }

    // Data
    if ($config['enable_data']) {
        $statData = new StatData();
        if ($config['data_files']) {
            File::output($statData->getInfoCuentas(), $pathData."/data_info_cuentas.tab", true, true);
            File::output($statData->getCargas(), $pathData."/data_cargas.tab", true);
            File::output($statData->getHistorico(), $pathData."/data_historico.tab", true);
            File::output($statData->getEpisodiosCodificados(), $pathData."/data_episodios_codificados.tab", true);
        }
        $statData->actualizarEpisodiosCodificados();
        $statData->actualizarDuracionGuardadoEpisodios();
    }

} catch (Exception $e) {
    echo $e->getMessage() . "\n";
}
