#! /bin/sh

# Inicialitzacions
data="/management/estadisticas/data"

# Obtenim les darreres estadistiques
darrer_fitxer=`ls -lart $data/ | grep estadistiques | tail -1 | grep -o estadistiques.*$`

# Comprimim les estadistiques
cd $data
tar cvfz $darrer_fitxer.tgz $darrer_fitxer

# Enviem les estadistiques per email
echo "En el fitxer adjunt hi ha les darreres estadistiques de Xile (MINSAL)." | mailx -a $darrer_fitxer.tgz -s "Estadistiques Xile (MINSAL)" iametrics_data@iasist.com

# Eliminem el fitxer comprimit
rm -f $darrer_fitxer.tgz
