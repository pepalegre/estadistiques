CREATE DATABASE estadisticas;

USE estadisticas;

CREATE TABLE iat_episodios_codificados (
  id                    INT UNSIGNED NOT NULL AUTO_INCREMENT,
  cuenta                VARCHAR(32),
  fecha_codificacion    DATE         NOT NULL,
  episodios_codificados INT UNSIGNED NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
