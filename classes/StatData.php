<?php

/**
 * Class StatData
 * Gestor d'estadístiques del Data
 */
class StatData
{
    /**
     * Retorna informació dels comptes
     * @return array Informació dels comptes
     * @throws Exception
     */
    public function getInfoCuentas()
    {
        Db::selectDb('clinos_plataforma');
        return Db::fetchAll(
            "SELECT id_cuenta_iametrics AS id_cuenta, nombre_bd, nombre_cuenta, descripcion " .
            "FROM iav_cuentas"
        );
    }

    /**
     * Retorna la llista de càrregues
     * @return array Llista de càrregues
     * @throws Exception
     */
    public function getCargas()
    {
        Db::selectDb('clinos_plataforma');
        $resultado = array();
        $filas = Db::fetchAll(
            "SELECT DISTINCT nombre_bd, nombre_cuenta " .
            "FROM iav_cuentas"
        );
        foreach ($filas as $fila) {
            Db::selectDb($fila['nombre_bd']);
            $rows = Db::fetchAll(
                "SELECT '" . $fila['nombre_cuenta'] . "' AS nombre_cuenta, id_carga, nombre_usuario, fecha_carga, fecha_publicacion, " .
                "fecha_primera, fecha_ultima, altas_cargadas, altas_procesadas, altas_descartadas, altas_existentes, " .
                "altas_publicadas, altas_eliminadas, descripcion_estructura, descripcion_formato, descripcion_estado " .
                "FROM iav_cargas " .
                "WHERE id_idioma = 0"
            );
            $resultado = array_merge($resultado, $rows);
        }
        return $resultado;
    }

    /**
     * Retorna els històrics
     * @return array Històrics
     * @throws Exception
     */
    public function getHistorico()
    {
        Db::selectDb('clinos_plataforma');
        $resultado = array();
        $filas = Db::fetchAll(
            "SELECT DISTINCT nombre_bd, nombre_cuenta " .
            "FROM iav_cuentas"
        );
        foreach ($filas as $fila) {
            Db::selectDb($fila['nombre_bd']);
            $rows = Db::fetchAll(
                "SELECT '" . $fila['nombre_cuenta'] . "' AS nombre_cuenta, COUNT(*) as num_episodios, COUNT(DISTINCT id_centro) as num_centros, " .
                "MIN(fecha_alta) AS fecha_alta_min, MAX(fecha_alta) AS max_fecha_alta " .
                "FROM iat_episodios " .
                "WHERE estado=1"
            );
            $resultado = array_merge($resultado, $rows);
        }
        return $resultado;
    }

    /**
     * Retorna els episodis codificats
     * @return array Episodis codificats
     * @throws Exception
     */
    public function getEpisodiosCodificados()
    {
        $resultado = array();
        Db::selectDb('clinos_plataforma');
        $bdcuentas = Db::getBDs('clinos_cuenta_');
        if (is_array($bdcuentas)) {
            foreach ($bdcuentas as $bdcuenta) {
                Db::selectDb($bdcuenta);
                $rows = Db::fetchAll(
                    "SELECT database() AS cuenta, cpf02 AS fecha_codificacion, count(*) AS episodios_codificados " .
                    "FROM iat_episodios_campos_propios AS iecp " .
                    "JOIN iat_episodios AS e USING(id_episodio) " .
                    "WHERE e.estado=1 " .
                    "GROUP BY cpf02"
                );
                $resultado = array_merge($resultado, $rows);
            }
        }
        return $resultado;
    }

    /**
     * Actualitza la taula d'episodis codificats (estadisticas.iat_episodios_codificados)
     * @throws Exception
     */
    public function actualizarEpisodiosCodificados()
    {
        Db::selectDb('clinos_plataforma');
        $bdcuentas = Db::getBDs('clinos_cuenta_');
        if (is_array($bdcuentas)) {
            Db::query(/** @lang text */
                "DELETE FROM estadisticas.iat_episodios_codificados"
            );
            Db::query(/** @lang text */
                "ALTER TABLE estadisticas.iat_episodios_codificados AUTO_INCREMENT=1"
            );
            $sql = /** @lang text */
                "INSERT INTO estadisticas.iat_episodios_codificados(cuenta, fecha_codificacion, episodios_codificados) " .
                "SELECT database(), date(cpf02), count(*) " .
                "FROM iat_episodios_campos_propios AS iecp " .
                "JOIN iat_episodios AS e USING(id_episodio) " .
                "WHERE e.estado=1 " .
                "GROUP BY date(cpf02)";
            foreach ($bdcuentas as $bdcuenta) {
                Db::selectDb($bdcuenta);
                Db::query($sql);
            }
        }
    }

    /**
     * Guardar la estadística de duración de episodios guardados.
     * @throws Exception
     */
    public function actualizarDuracionGuardadoEpisodios()
    {
        Db::selectDb('clinos_plataforma');
        $cuentas = Db::fetchAll("select nombre_bd from iav_cuentas where id_supercuenta=377");
        if (is_array($cuentas)) {
            foreach ($cuentas as $cuenta) {
                Db::selectDb($cuenta["nombre_bd"]);
                $sql = /** @lang MySQL */
                    "replace into estadisticas.iat_duracion_guardado_episodios (bd_cuenta, fecha, segundos, llamadas, promedio)
                    select database(), date(fecha), 
                    sum(cast(mid(descripcion, locate(':', descripcion)+2, locate(' segundos', descripcion)-(locate(':', descripcion)+2)) as UNSIGNED)) as segundos,
                    sum(1) as llamadas,
                    (sum(cast(mid(descripcion, locate(':', descripcion)+2, locate(' segundos', descripcion)-(locate(':', descripcion)+2)) as UNSIGNED)) / sum(1)) as promedio
                    from iat_log 
                    where date(fecha)=date(subdate(now(), 1)) and (descripcion like 'Duraci%n guardado episodio%segundos.')
                    group by date(fecha)";
                Db::query($sql);
                $sql = /** @lang MySQL */
                    "replace into estadisticas.iat_duracion_guardado_episodios_grd (bd_cuenta, fecha, segundos, llamadas, promedio)
                    select database(), date(fecha), 
                    sum(cast(mid(descripcion, locate(':', descripcion)+2, locate(' segundos', descripcion)-(locate(':', descripcion)+2)) as UNSIGNED)) as segundos,
                    sum(1) as llamadas,
                    (sum(cast(mid(descripcion, locate(':', descripcion)+2, locate(' segundos', descripcion)-(locate(':', descripcion)+2)) as UNSIGNED)) / sum(1)) as promedio
                    from iat_log 
                    where date(fecha)=date(subdate(now(), 1)) and (descripcion like 'Duraci%n guardado episodio%GRD).')
                    group by date(fecha)";
                Db::query($sql);

                // Añadidas horas
                $sql = /** @lang MySQL */
                    "replace into estadisticas.iat_duracion_guardado_episodios2 (bd_cuenta, fecha, hora, segundos, llamadas, promedio)
                    select database(), date(fecha), hour(fecha),
                    sum(cast(mid(descripcion, locate(':', descripcion)+2, locate(' segundos', descripcion)-(locate(':', descripcion)+2)) as UNSIGNED)) as segundos,
                    sum(1) as llamadas,
                    (sum(cast(mid(descripcion, locate(':', descripcion)+2, locate(' segundos', descripcion)-(locate(':', descripcion)+2)) as UNSIGNED)) / sum(1)) as promedio
                    from iat_log 
                    where date(fecha)=date(subdate(now(), 1)) and (descripcion like 'Duraci%n guardado episodio%segundos.')
                    group by date(fecha), hour(fecha)";
                Db::query($sql);
                $sql = /** @lang MySQL */
                    "replace into estadisticas.iat_duracion_guardado_episodios_grd2 (bd_cuenta, fecha, hora, segundos, llamadas, promedio)
                    select database(), date(fecha), hour(fecha), 
                    sum(cast(mid(descripcion, locate(':', descripcion)+2, locate(' segundos', descripcion)-(locate(':', descripcion)+2)) as UNSIGNED)) as segundos,
                    sum(1) as llamadas,
                    (sum(cast(mid(descripcion, locate(':', descripcion)+2, locate(' segundos', descripcion)-(locate(':', descripcion)+2)) as UNSIGNED)) / sum(1)) as promedio
                    from iat_log 
                    where date(fecha)=date(subdate(now(), 1)) and (descripcion like 'Duraci%n guardado episodio%GRD).')
                    group by date(fecha), hour(fecha)";
                Db::query($sql);
            }
        }
    }
}
