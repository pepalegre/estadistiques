<?php

/**
 * Class StatIametrics
 * Gestor d'estadístiques d'IAmetrics
 */
class StatIametrics
{
    /**
     * Retorna la llista de logins
     * @return array Llista de logins
     * @throws Exception
     */
    public function getLogins()
    {
        Db::selectDb('iametrics');
        return Db::fetchAll(
            "SELECT * " .
            "FROM iat_logins"
        );
    }

    /**
     * Retorna la llista de comptes
     * @return array Llista de comptes
     * @throws Exception
     */
    public function getCuentas()
    {
        Db::selectDb('iametrics');
        return Db::fetchAll(
            "SELECT * " .
            "FROM iat_cuentas " .
            "WHERE id_cuenta >= 0"
        );
    }

    /**
     * Retorna la llista d'usuaris
     * @return array Llista d'usuaris
     * @throws Exception
     */
    public function getUsuarios()
    {
        Db::selectDb('iametrics');
        return Db::fetchAll(
            "SELECT * " .
            "FROM iat_usuarios"
        );
    }

    /**
     * Retorna la llista d'usuaris de cada aplicació i compte
     * @return array Llista d'usuaris de cada aplicació i compte
     * @throws Exception
     */
    public function getUsuariosAplicacionCuenta()
    {
        Db::selectDb('iametrics');
        return Db::fetchAll(
            "SELECT uac.*, u.alias AS alias_usuario, u.nombre AS nombre_usuario, da.descripcion AS descripcion_aplicacion, c.descripcion AS descripcion_cuenta " .
            "FROM iat_usuarios_aplicacion_cuenta AS uac " .
            "JOIN iat_usuarios AS u USING (id_usuario) " .
            "JOIN iat_aplicaciones AS a USING(id_aplicacion) " .
            "JOIN iat_def_aplicaciones AS da on(a.id_aplicacion=da.id_aplicacion and da.id_idioma=0) " .
            "JOIN iat_cuentas AS c USING(id_cuenta)"
        );
    }

    /**
     * Retorna la llista de claus de desencriptació
     * @return array Llista de claus de desencriptació
     * @throws Exception
     */
    public function getClaves()
    {
        Db::selectDb('iametrics');
        return Db::fetchAll(
            "SELECT * " .
            "FROM iat_claves"
        );
    }
}
