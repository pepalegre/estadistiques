<?php

/**
 * Class StatGPS
 * Gestor d'estadístiques del GPS
 */
class StatGPS
{
    /**
     * Llista de comptes indexat pel nom de la BDCuenta
     * @var array
     */
    protected $cuentas;

    /**
     * StatGPS constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->cuentas = $this->getCuentasPorNombresBDCuenta();
    }

    /**
     * Retorna la llista de logins de GPS
     * @return array Llista de logins de GPS
     * @throws Exception
     */
    public function getLogins()
    {
        Db::selectDb('iametrics');
        return Db::fetchAll(
            "SELECT * " .
            "FROM iat_logins " .
            "WHERE aplicacion = 'gps'"
        );
    }

    /**
     * Retorna la llista d'accessos a dades personals de GPS
     * @return array Llista d'accessos a dades personals de GPS
     * @throws Exception
     */
    public function getDatosPersonales()
    {
        Db::selectDb('iametrics_framework');
        return Db::fetchAll(
            "SELECT * " .
            "FROM iat_datos_personales " .
            "WHERE id_aplicacion = 26"
        );
    }

    /**
     * Retorna la llista de comptes de GPS
     * @return array Llista de comptes de GPS
     * @throws Exception
     */
    public function getCuentas()
    {
        Db::selectDb('iametrics');
        return Db::fetchAll(
            "SELECT * " .
            "FROM iat_cuentas " .
            "JOIN iat_aplicacion_cuenta USING(id_cuenta) " .
            "WHERE id_cuenta >= 0 and id_aplicacion = 26"
        );
    }

    /**
     * Retorna la llista de càrregues incrementals de GPS
     * @return array Llista de càrregues incrementals de GPS
     * @throws Exception
     */
    public function getQueueBDCuentasInc()
    {
        Db::selectDb('gps_plataforma_new');
        $rows = Db::fetchAll(
            "SELECT nombre_bd,fecha_cola,fecha_inicial,fecha_final,fecha_traspaso,estado,carga " .
            "FROM iat_queue_bdcuentas_inc"
        );
        if (!is_null($rows)) {
            foreach ($rows as $index => $row) {
                if (isset($this->cuentas[$row['nombre_bd']])) {
                    $rows[$index]['cuenta'] = $this->cuentas[$row['nombre_bd']];
                } else {
                    $rows[$index]['cuenta'] = "[no asignada]";
                }
                $rows[$index]['traspasado'] = trim($row['fecha_traspaso']) == '' ? 0 : 1;
            }
        }
        return $rows;
    }

    /**
     * Retorna la llista d'usuaris de cada compte
     * @return array Llista d'usuaris de cada compte
     * @throws Exception
     */
    public function getUsuariosCuentas()
    {
        $usuarios = array();
        $bdcuentas = $this->getBDCuentas();
        if (!is_null($bdcuentas)) {
            foreach ($bdcuentas as $bdcuenta) {
                Db::selectDb($bdcuenta);
                $rows = Db::fetchAll(
                    "SELECT * " .
                    "FROM iat_configuracion_usuarios " .
                    "JOIN iametrics.iat_usuarios USING(id_usuario)"
                );
                if (!is_null($rows)) {
                    foreach ($rows as $row) {
                        if (isset($this->cuentas[$bdcuenta])) {
                            $row['cuenta'] = $this->cuentas[$bdcuenta];
                        } else {
                            $row['cuenta'] = "[no asignada]";
                        }
                        $usuarios[] = $row;
                    }
                }
            }
        }
        return $usuarios;
    }

    /**
     * Retorna els periodes que tenen episodis de cada centre de cada compte
     * @param bool $admin Taula "_ADMIN"?
     * @return array Periodes que tenen episodis de cada centre de cada compte
     * @throws Exception
     */
    public function getPeriodosCuentaCentro($admin)
    {
        $periodos = array();
        $centrosPorId = $this->getCentrosPorId();
        $tabla = $admin ? "GPST_CTA_EPS_INDEX_TEMPORAL_ADMIN" : "GPST_CTA_EPS_INDEX_TEMPORAL";
        $bdDatosCuentas = $this->getBDDatosCuentas();
        if (!is_null($bdDatosCuentas)) {
            foreach ($bdDatosCuentas as $bdDatosCuenta) {
                Db::selectDb($bdDatosCuenta);
                try {
                    $rows = Db::fetchAll(
                        "SELECT id_centro, AA, MA, Episodios, FechaMaxima " .
                        "FROM " . $tabla
                    );

                    foreach ($rows as $row) {
                        $row['descripcion'] = '';
                        if (isset($centrosPorId[$row['id_centro']]['descripcion'])) {
                            $row['descripcion'] = $centrosPorId[$row['id_centro']]['descripcion'];
                        }
                        $bdcuenta = str_replace('datos_', '', $bdDatosCuenta);
                        if (isset($this->cuentas[$bdcuenta])) {
                            $row['cuenta'] = $this->cuentas[$bdcuenta];
                        } else {
                            $row['cuenta'] = "[no asignada]";
                        }
                        $periodos[] = $row;
                    }
                } catch (Exception $e) {
                }
            }
        }
        return $periodos;
    }

    /**
     * Retorna la llista de centres indexats per l'id
     * @return array Llista de centres indexats per l'id
     * @throws Exception
     */
    protected function getCentrosPorId()
    {
        $centros = array();
        Db::selectDb('gps_plataforma_new');
        $rows = Db::fetchAll(
            "SELECT * " .
            "FROM iat_centros"
        );
        if (!is_null($rows)) {
            foreach ($rows as $row) {
                $centros[$row['id_centro']] = $row;
            }
        }
        return $centros;
    }

    /**
     * Retorna la llista de "gps3_cuenta_"
     * @return array Llista de "gps3_cuenta_"
     * @throws Exception
     */
    protected function getBDCuentas()
    {
        Db::selectPdo(false);
        return Db::getBDs('gps3_cuenta_');
    }

    /**
     * Retorna la llista de "gps3_datos_cuenta_"
     * @return array Llista de "gps3_datos_cuenta_"
     * @throws Exception
     */
    protected function getBDDatosCuentas()
    {
        Db::selectPdo(true);
        return Db::getBDs('gps3_datos_cuenta_');
    }

    /**
     * Retorna la llista de comptes indexades pel nom de la BDCuenta
     * @return array Llista de comptes indexades pel nom de la BDCuenta
     * @throws Exception
     */
    protected function getCuentasPorNombresBDCuenta()
    {
        $cuentas = null;
        Db::selectDb('gps_plataforma_new');
        $rows = Db::fetchAll(
            "SELECT co.nombre_bd, icu.nombre " .
            "FROM iat_cuentas gcu " .
            "JOIN iametrics.iat_cuentas icu USING (id_cuenta) " .
            "JOIN iat_conexiones co ON (gcu.id_conexion_bdcuenta = co.id)"
        );
        if (!is_null($rows)) {
            foreach ($rows as $index => $row) {
                $cuentas[$row['nombre_bd']] = $row['nombre'];
            }
        }
        return $cuentas;
    }
}
