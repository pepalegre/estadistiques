<?php

/**
 * Class Config
 * Gestor de configuració
 */
abstract class Config
{
    /**
     * Retorna la configuració per l'entorn actual (APPLICATION_ENV)
     * @return array Configuració per l'entorn actual (APPLICATION_ENV)
     * @throws Exception
     */
    public static function get()
    {
        $ini = parse_ini_file(__DIR__ . '/../config/config.ini', true);
        $env = (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'prod');
        if (!isset($ini[$env])) {
            throw new Exception("El entorno '" . $env . "' no está definido en la configuración!");
        }
        $iniEnv = array_merge($ini['_shared_'], $ini[$env]);
        self::evalTags($iniEnv);
        return $iniEnv;
    }

    /**
     * Evalua els tags predefinits ([NOM_TAG])
     * @param array $array Llista de paràmetres
     */
    protected static function evalTags(&$array)
    {
        foreach ($array as $key => $value) {
            if (preg_match('/\[([^\]]+)\]/', $value, $matches)) {
                switch ($matches[1]) {
                    case "DATE":
                        $array[$key] = str_replace($matches[0], date("Ymd"), $value);
                }
            }
        }
    }
}
