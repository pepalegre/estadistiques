<?php

/**
 * Class File
 * Gestor de fitxers
 */
abstract class File
{
    /**
     * Escriu al fitxer de sortida les files
     * @param array $rows Files
     * @param string $filename Nom del fitxer de sortida
     * @param bool $appendHeader Afegir la capçalera al fitxer?
     * @param bool $utf8encode Sortida en format UTF-8?
     */
    public static function output($rows, $filename, $appendHeader, $utf8encode = false)
    {
        // Obrim el fitxer de sortida
        if (!$fd = fopen($filename, "w")) {
            die("Error: no puc obrir el fitxer '" . $filename . "' en mode 'w'\r\n");
        }

        // Escribim les files
        if (is_array($rows) && !empty($rows)) {
            if ($appendHeader) {
                $line = implode("\t", array_keys($rows[0]));
                $utf8encode || $line = utf8_decode($line);
                fwrite($fd, $line . "\r\n") || die("Error: no puc escriure la capçalera al fitxer!\r\n");
            }
            foreach ($rows as $row) {
                $line = implode("\t", array_values($row));
                $utf8encode || $line = utf8_decode($line);
                fwrite($fd, $line . "\r\n") || die("Error: no puc escriure la fila al fitxer!\r\n");
            }
        }

        // Tanquem el fitxer de sortida
        if (!fclose($fd)) {
            die("Error: no puc tancar el fitxer!\r\n");
        }
    }
}
