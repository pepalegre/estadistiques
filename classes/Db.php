<?php

/**
 * Class Db
 * Gestor de BDs
 */
abstract class Db
{
    /**
     * Pdo principal i el de gps3_datos_cuenta_...
     * @var PDO[]
     */
    protected static $pdos;
    /**
     * Pdo actual
     * @var PDO
     */
    protected static $currentPdo;

    /**
     * Selecciona una BD, si aquesta comença per "gps3_datos_cuenta_..." ja canvia de PDO automàticament
     * @param string $db Nom de la BD a connectar-se
     * @throws Exception
     */
    public static function selectDb($db)
    {
        $gps3DatosCuenta = preg_match('/^gps3_datos_cuenta_/', $db);
        self::$currentPdo = self::selectPdo($gps3DatosCuenta);
        $pdo = self::$currentPdo;
        $pdo->exec("use " . $db);
    }

    /**
     * Selecciona el PDO on es troben les BDs principals o el PDO de gps3_datos_cuenta_...
     * @param bool $gps3DatosCuenta Volem el PDO de gps3_datos_cuenta_... o el principal?
     * @return PDO PDO on es troben les BDs principals o el PDO de gps3_datos_cuenta_...
     * @throws Exception
     */
    public static function selectPdo($gps3DatosCuenta = false)
    {
        // Si el PDO no està instanciat, l'instanciem
        if (!isset(self::$pdos[intval($gps3DatosCuenta)])) {

            // Obtenim la configuració general i adaptem el paràmetre "host"
            $config = Config::get();
            if ($gps3DatosCuenta) {
                $config['host'] = $config['host_gps3_datos_cuenta'];
            }
            unset($config['host_gps3_datos_cuenta']);

            // Instanciem la connexió al gestor de BD
            self::$pdos[intval($gps3DatosCuenta)] = new PDO(
                self::getDsn($config),
                $config['user'],
                $config['pass'],
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => true
                ]
            );
        }

        // Tornem el PDO
        return self::$pdos[intval($gps3DatosCuenta)];
    }

    /**
     * Executa la consulta SQL
     * @param string $sql Consulta SQL
     * @return PDOStatement
     */
    public static function query($sql)
    {
        $pdo = self::$currentPdo;
        return $pdo->query($sql);
    }

    /**
     * Retorna totes les tuples de la consulta SQL passada per paràmetre
     * @param string $sql Consulta SQL
     * @param bool $assoc Retornar les columnes pel seu nom o índexos numèrics?
     * @return array Totes les tuples de la consulta SQL passada per paràmetre
     */
    public static function fetchAll($sql, $assoc = true)
    {
        $pdo = self::$currentPdo;
        return $pdo
            ->query($sql)
            ->fetchAll($assoc ? PDO::FETCH_ASSOC : PDO::FETCH_NUM);
    }

    /**
     * Retorna tots els noms de Bds que compleixen amb el prefix
     * @param string $prefijo Prefix
     * @return array Tots els noms de Bds que compleixen amb el prefix
     */
    public static function getBDs($prefijo)
    {
        $bdcuentas = null;
        $rows = self::fetchAll(
            "show databases like '" . $prefijo . "%'",
            false
        );
        if (is_array($rows)) {
            foreach ($rows as $index => $row) {
                $bdcuentas[$index] = $row[0];
            }
        }
        return $bdcuentas;
    }

    /**
     * Retorna el DSN de connexió al gestor de BD
     * @param array $config Ha de contenir els paràmetres : driver, host i port
     * @return string DSN de connexió al gestor de BD
     */
    protected static function getDsn($config)
    {
        return
            $config['driver'] . ':' .
            "host=" . $config['host'] . ";" .
            "port=" . $config['port'] . ";" .
            "charset=utf8";
    }
}
